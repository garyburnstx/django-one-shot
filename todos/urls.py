from django.urls import path, include
from todos.views import *

urlpatterns = [
    path('', show_list_item, name='todo_list_list'),
    path('<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('create/', create_todo_list, name='todo_list_create'),
    path('<int:id>/edit/', edit_todo_list, name='todo_list_update'),
    path('<int:id>/delete/', delete_todo_list, name='todo_list_delete'),
    path('items/create/', create_todo_item, name='todo_item_create'),
    path('items/<int:id>/edit/', update_todo_item, name='todo_item_update'),
    ]
