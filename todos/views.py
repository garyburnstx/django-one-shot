from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.
def show_list_item(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list":todo_list_list
    }
    return render(request, "todos/todo_lists.html", context)

def todo_list_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    todo_list_tasks = todo_list_detail.items.all()
    context = {
        "todo_list_detail":todo_list_detail,
        "todo_list_tasks":todo_list_tasks
    }
    return render(request, "todos/todo_list_detail.html", context)

def create_todo_list(request):

    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
        # To redirect to the detail view of the model, use this:
            list = form.save()
            return redirect("todo_list_detail", list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {
    "form": form
    }

    return render(request, "todos/todo_list_create.html", context)

def edit_todo_list(request, id):
    todolist_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist_instance)
        if form.is_valid():
            todolist_instance = form.save()
            return redirect("todo_list_detail", id=todolist_instance.id)

    else:
       form = TodoListForm(instance=todolist_instance)
    return render(request, "todos/todo_list_update.html")


def delete_todo_list(request, id):
    todolist_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")


def create_todo_item(request):

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
        # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", item.list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoItemForm()

    context = {
    "form": form
    }

    return render(request, "todos/todo_item_create.html", context)

def update_todo_item(request, id):
    todoitem_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem_instance)
        if form.is_valid():
            todoitem_instance = form.save()
            return redirect("todo_list_detail", id=todoitem_instance.list.id)

    else:
        form = TodoItemForm(instance = todoitem_instance)

    context = {
        "form": form
    }

    return render(request, "todos/todo_item_update.html", context)
